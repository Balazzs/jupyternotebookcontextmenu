# JupyterNotebookContextMenu

A context menu option (shift + right click in folder) for windows for running a Jupyter notebook server from a directory without trashing it.

Set a notebook directory for storing notebooks, the script generates a notebook with a unique name for each working directory (the directory where you right clicked) and adds a chdir command to it (the notebook's default directory is the notebook directory).

The server is started with kernel idle timeout and notebook idle shutdown time of 1 day.

## Install

1. copy the script and the folder with the template to a permanent location. all your notebooks will be there

2. edit the script, change the notebook_dir variable to your directory

3.  edit the .reg file, set the python executable location to your python.exe location, set the script location

4. import the registry key (run it)