import os
import hashlib
import shutil
import re

notebook_dir = "D:/IPython/dir_notebooks/"

directory = os.getcwd()
filename = hashlib.sha256(directory.encode()).hexdigest() + ".ipynb"

template_path = os.path.join(notebook_dir, "template.ipynb")
notebook_path = os.path.join(notebook_dir, filename)

if not os.path.isfile(notebook_path):
	with open(notebook_path, "w") as notebook:
		with open(template_path, "r") as template:
			notebook.write(template.read().replace("<<CWD>>", r'r\"%s\"' % re.sub(r"[\\\/]", r"\\\\", directory)))

os.system('jupyter notebook %s --notebook-dir="%s" --NotebookApp.shutdown_no_activity_timeout=86400 --MappingKernelManager.cull_idle_timeout=86400' % (notebook_path, notebook_dir))
#os.system("pause")#debug